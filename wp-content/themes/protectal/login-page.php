<!-- <h2>Login</h2>
<form action="<?php echo get_option('home'); ?>/wp-login.php" method="post">
<input type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="20" />
<input type="password" name="pwd" id="pwd" size="20" />
<input type="submit" name="submit" value="Send" class="button" />
    <p>
       <label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me</label>
       <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
    </p>
</form>
<a href="<?php echo get_option('home'); ?>/wp-login.php?action=lostpassword">Recover password</a>
-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl-PL">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<title>Protectal</title>
	<link rel="stylesheet" id="wp-admin-css" href="<?php bloginfo('url'); ?>/wp-admin/css/wp-admin.min.css?ver=3.5" type="text/css" media="all">
    <link rel="stylesheet" id="buttons-css" href="<?php bloginfo('url'); ?>/wp-includes/css/buttons.min.css?ver=3.5" type="text/css" media="all">
    <link rel="stylesheet" id="colors-fresh-css" href="<?php bloginfo('url'); ?>/wp-admin/css/colors-fresh.min.css?ver=3.5" type="text/css" media="all">
    <link rel="stylesheet" id="custom_wp_admin_css" href="<?php bloginfo('url'); ?>/wp-content/themes/protectal/css/style-login.css" type="text/css" media="all">
    
    
    <link rel="stylesheet" id="custom_wp_admin_css" href="<?php bloginfo('template_directory'); ?>/includes/audiojs/audio.css" type="text/css" media="all">
    
    <style type="text/css">
	    .login h1 a { background-image:url(<?php bloginfo('template_directory'); ?>/images/logo.png) !important; }
	</style><meta name="robots" content="noindex,nofollow">
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.ubaplayer.js" async=""></script>
	<script>
	    $(function(){
			$("#ubaPlayer").ubaPlayer({	
			          autoPlay: $('.audioButton').eq(0)
			     });
	    });
	</script>
	
	
	<script src="http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" type="text/javascript" async=""></script>
	<script>
				WebFontConfig = {
				    google: { families: ['Coda', 'Coda', 'Open Sans',  'Vidaloka'] }
				};
				
				(function() {
					document.getElementsByTagName("html")[0].setAttribute("class","wf-loading")
					//  NEEDED to push the wf-loading class to your head
					document.getElementsByTagName("html")[0].setAttribute("className","wf-loading")
					// for IE…
				
				var wf = document.createElement('script');
					wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					 '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
					wf.type = 'text/javascript';
					wf.async = 'false';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(wf, s);
				})();
			</script>
</head>

	<body class="login login-action-login wp-core-ui">
	<div id="login">
		<h1><a href="<?php echo home_url(); ?>">Protectal</a></h1>

    <form action="<?php echo get_option('home'); ?>/wp-login.php" method="post">
    	<p>
    		<label for="user_login">Nazwa użytkownika<br>
    		<input type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="20" />
    		</label>
    	</p>
    	<p>
    		<label for="user_pass">Hasło<br>
    		<input type="password" name="pwd" id="pwd" class="input" size="20" />
    		</label>
    	</p>
    	
    	<input type="submit" name="submit" value="Zaloguj się" class="button button-primary button-large" />
    	    <p>
    	       <label for="rememberme">
    	           <input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Zapamiętaj mnie</label>
    	           <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
    	    </p>
    	    <p>
    	        <a class="login_contact" href="<?php bloginfo('url'); ?>/strona-kontaktowa">Nie posiadasz dostępu poproś o kontakti ></a>
    	    </p>
    </form>

        <script type="text/javascript">
            function wp_attempt_focus(){
            setTimeout( function(){ try{
            d = document.getElementById('user_login');
            d.focus();
            d.select();
            } catch(e){}
            }, 200);
            }
            
            if(typeof wpOnload=='function')wpOnload();
        </script>

	
	</div>

	<div id="login_footer">
	    <div id="login_footer_wrapper">
	        <?php dynamic_sidebar('Sidebar Two'); ?>
	    </div>
	</div>
	
	<div id="ubaPlayer">
	    <ul class="controls">
	        <li><a class="audioButton" href="<?php bloginfo('template_directory'); ?>/images/main_title.mp3">BG Music</a></li>
	    </ul>
	</div>
	
	<script style="display: none; " id="hiddenlpsubmitdiv"></script>
	
	<script>
	try{for(var lastpass_iter=0; lastpass_iter < document.forms.length; lastpass_iter++){ var lastpass_f = document.forms[lastpass_iter]; if(typeof(lastpass_f.lpsubmitorig2)=="undefined"){ lastpass_f.lpsubmitorig2 = lastpass_f.submit; lastpass_f.submit = function(){ var form=this; var customEvent = document.createEvent("Event"); customEvent.initEvent("lpCustomEvent", true, true); var d = document.getElementById("hiddenlpsubmitdiv"); for(var i = 0; i < document.forms.length; i++){ if(document.forms[i]==form){ d.innerText=i; } } d.dispatchEvent(customEvent); form.lpsubmitorig2(); } } }}catch(e){}
	</script>
	<script>
			    $(".stretchMe").anystretch();
		</script>
	
	</body>
	
	</html>