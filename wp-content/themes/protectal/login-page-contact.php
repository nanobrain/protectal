<?php
/*
Template Name: Startowa strona kontaktowa
*/
?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pl-PL">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    	<title>Protectal</title>
    	<link rel="stylesheet" id="wp-admin-css" href="<?php bloginfo('url'); ?>/wp-admin/css/wp-admin.min.css?ver=3.5" type="text/css" media="all">
        <link rel="stylesheet" id="buttons-css" href="<?php bloginfo('url'); ?>/wp-includes/css/buttons.min.css?ver=3.5" type="text/css" media="all">
        <link rel="stylesheet" id="colors-fresh-css" href="<?php bloginfo('url'); ?>/wp-admin/css/colors-fresh.min.css?ver=3.5" type="text/css" media="all">
	    <link rel="stylesheet" id="custom_wp_admin_css" href="<?php bloginfo('url'); ?>/wp-content/themes/protectal/css/style-login.css" type="text/css" media="all">
	    <style type="text/css">
		    .login h1 a { background-image:url(<?php bloginfo('url'); ?>/wp-content/uploads/2013/05/logo1.png) !important; background-size: auto; }
		</style><meta name="robots" content="noindex,nofollow">
		<script src="http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" type="text/javascript" async=""></script>
		<script>
			WebFontConfig = {
			    google: { families: ['Orbitron', 'Orbitron', 'Open Sans',  'Vidaloka'] }
			};
			
			(function() {
				document.getElementsByTagName("html")[0].setAttribute("class","wf-loading")
				//  NEEDED to push the wf-loading class to your head
				document.getElementsByTagName("html")[0].setAttribute("className","wf-loading")
				// for IE…
			
			var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				 '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'false';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
		</script>
	</head>
	
	<body class="login login-action-login wp-core-ui">
	<div id="login" class="login-contact-wrap">
		<h1><a href="<?php echo home_url(); ?>">Protectal</a></h1>
        <div id="login-contact">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; // end of the loop. ?>
        </div>
	
	</div>

	<div id="login_footer">
	    <div id="login_footer_wrapper">
	        <?php dynamic_sidebar('Sidebar Two'); ?>
	    </div>
	</div>
	

	</body>
	
	</html>